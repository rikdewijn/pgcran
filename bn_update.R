
options(repos = c("https://r.pamgene.com/pgcran",
                  "https://r.pamgene.com/cran"))

installBNPackages = function(){
  installBiobase('Biobase')
  installBiobase('vsn')
  installPackage("tibble", "1.2")
  installPackage("ggplot2", "2.2.1")
  installPackage("shiny", "1.0.1")
  installPackage("bnutil", "2.4")
  installPackage("bnshiny", "2.25")
  installPackage("pgCheckInput", "3.5")
  installPackage("PamCloud", "1.4" )
  installPackage("pgMulticore", "3.3")
}

installBiobase = function(pkg){
  if (! (pkg %in% installed.packages()))  {
    source("https://bioconductor.org/biocLite.R")
    biocLite(pkg)
  }
}

installPackage = function(pkg, version){
  if (! (pkg %in% installed.packages()))  {
    install.packages(pkg)
  } 
  
  if (packageVersion(pkg) < package_version(version)){
    install.packages(pkg)
  }
  if (packageVersion(pkg) < package_version(version)){
    stop(paste('Failed to install package',pkg,'with version constraint >= ', version, 'current installed version', packageVersion(pkg)))
  }
}

installBNPackages()
library(Biobase)